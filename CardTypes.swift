//
//  CardTypes.swift
//  CitizensParking
//
//  Created by Mouriuser on 08/05/19.
//  Copyright © 2019 Citizens Parking. All rights reserved.
//

import Foundation

class cardTypes: NSObject {
    
    static let sharedInstance =  cardTypes()

    
    func validateCardType(testCard: String) -> String {
        let regVisa = "^4[0-9]{12}(?:[0-9]{3})?$"
        let regMaster = "^5[1-5][0-9]{14}$"
        let regExpress = "^3[47][0-9]{13}$"
        let regDiners = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$"
        let regDiscover = "^6(?:011|5[0-9]{2})[0-9]{12}$"
        let regRupay = "^6[0-9]{15}$"
        let regAmexCard = "^3[47][0-9]{13}$"
        let regBCGlobal = "^(6541|6556)[0-9]{12}$"
        let regCarteBlancheCard = "^389[0-9]{11}$"
        let regDinersClubCard = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$"
        let regDiscoverCard = "^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$"
        let regInstaPaymentCard = "^63[7-9][0-9]{13}$"
        let regJCBCard = "^(?:2131|1800|35{3}){11}$"
        let regKoreanLocalCard = "^9[0-9]{15}$"
        let regLaserCard = "^(6304|6706|6709|6771)[0-9]{12,15}$"
        let regMaestroCard = "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
        let regMastercard = "^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$"
        let regSoloCard = "^(6334|6767)[0-9]{12}|(6334|6767)[0-9]{14}|(6334|6767)[0-9]{15}$"
        let regSwitchCard = "^(4903|4905|4911|4936|6333|6759)[0-9]{12}|(4903|4905|4911|4936|6333|6759)[0-9]{14}|(4903|4905|4911|4936|6333|6759)[0-9]{15}|564182[0-9]{10}|564182[0-9]{12}|564182[0-9]{13}|633110[0-9]{10}|633110[0-9]{12}|633110[0-9]{13}$"
        let regUnionPayCard = "^(62[0-9]{14,17})$"
        let regVisaMasterCard =  "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$"
        
        
        let regVisaTest = NSPredicate(format: "SELF MATCHES %@", regVisa)
        let regMasterTest = NSPredicate(format: "SELF MATCHES %@", regMaster)
        let regExpressTest = NSPredicate(format: "SELF MATCHES %@", regExpress)
        let regDinersTest = NSPredicate(format: "SELF MATCHES %@", regDiners)
        let regDiscoverTest = NSPredicate(format: "SELF MATCHES %@", regDiscover)
        let regRupayTest = NSPredicate(format: "SELF MATCHES %@", regRupay)
        
        let regAmexCardTest = NSPredicate(format: "SELF MATCHES %@", regAmexCard)
        let regBCGlobalTest = NSPredicate(format: "SELF MATCHES %@", regBCGlobal)
        let regCarteBlancheCardTest = NSPredicate(format: "SELF MATCHES %@", regCarteBlancheCard)
        let regDiscoverCardTest = NSPredicate(format: "SELF MATCHES %@", regDiscoverCard)
        let regInstaPaymentCardTest = NSPredicate(format: "SELF MATCHES %@", regInstaPaymentCard)
        let regDinersClubCardTest = NSPredicate(format: "SELF MATCHES %@", regDinersClubCard)
        let regJCBCardTest = NSPredicate(format: "SELF MATCHES %@", regJCBCard)
        let regKoreanLocalCardTest = NSPredicate(format: "SELF MATCHES %@", regKoreanLocalCard)
        let regLaserCardTest = NSPredicate(format: "SELF MATCHES %@", regLaserCard)
        let regMaestroCardTest = NSPredicate(format: "SELF MATCHES %@", regMaestroCard)
        let regMastercardTest = NSPredicate(format: "SELF MATCHES %@", regMastercard)
        let regSoloCardTest = NSPredicate(format: "SELF MATCHES %@", regSoloCard)
        let regSwitchCardTest = NSPredicate(format: "SELF MATCHES %@", regSwitchCard)
        let regUnionPayCardTest = NSPredicate(format: "SELF MATCHES %@", regSwitchCard)
        let regVisaMasterCardTest = NSPredicate(format: "SELF MATCHES %@", regVisaMasterCard)

        
        if regVisaTest.evaluate(with: testCard){
            return "Visa Card"
        }
        else if regMasterTest.evaluate(with: testCard){
            return "Master Card"
        }
        else if regExpressTest.evaluate(with: testCard){
            return "American Express"
        }
        else if regDinersTest.evaluate(with: testCard){
            return "Diners Club"
        }
        else if regDiscoverTest.evaluate(with: testCard){
            return "Discover"
        }
        else if regRupayTest.evaluate(with: testCard){
            return "RuPay"
        }
        else if regAmexCardTest.evaluate(with: regAmexCard){
            return "Amex Card"
        }
        else if regBCGlobalTest.evaluate(with: regBCGlobal){
            return "BCGlobal"
        }
        else if regCarteBlancheCardTest.evaluate(with: regCarteBlancheCard){
            return "Carte Blanche Card"
        }
        else if regDiscoverCardTest.evaluate(with: regDiscoverCard){
            return "Discover Card"
        }
        else if regInstaPaymentCardTest.evaluate(with: regInstaPaymentCard){
            return "Insta Payment Card"
        }
        else if regDinersClubCardTest.evaluate(with: regDinersClubCard){
            return "Diners Club Card"
        }
        else if regJCBCardTest.evaluate(with: regJCBCard){
            return "JCB Card"
        }
        else if regKoreanLocalCardTest.evaluate(with: regKoreanLocalCard){
            return "KoreanLocalCard"
        }
        else if regLaserCardTest.evaluate(with: regLaserCard){
            return "Laser Card"
        }
        else if regMaestroCardTest.evaluate(with: regMaestroCard){
            return "Maestro Card"
        }
        else if regMastercardTest.evaluate(with: regMastercard){
            return "Master Card"
        }
        else if regSoloCardTest.evaluate(with: regSoloCard){
            return "Solo Card"
        }
        else if regSwitchCardTest.evaluate(with: regSwitchCard){
            return "Switch Card"
        }
        else if regUnionPayCardTest.evaluate(with: regUnionPayCard){
            return "Union Pay Card"
        }
        else if regVisaMasterCardTest.evaluate(with: regVisaMasterCard){
            return "Visa Master Card"
        }
        return ""
        
    }
    
}
