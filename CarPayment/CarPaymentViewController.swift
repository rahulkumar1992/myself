//
//  CarPaymentViewController.swift
//  CitizensParking
//
//  Created by NalaN on 3/10/19.
//  Copyright © 2019 Citizens Parking. All rights reserved.
//

import UIKit
import CoreLocation

let kName = "Name"
let kPaymentColor = "PaymentColor"
let kStatus = "Status"
let kInprogress = "InProgress"
let TOTAL = 100


class CarPaymentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    // let lib = DTDevices.sharedDevice() as! DTDevices
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    
    var paymentList = [[String: Any]]()
    
    var strProcessCompletion = NSString()
    
    let btn = UIButton(type: .custom)
    
    var errorcheck = String()
    
    var locm:CLLocationManager?
    
    var location:CLLocation?
    
    @IBOutlet weak var abortBtn: UIButton!
    
    var dueAmount : Double? = 0
    
    let infinea  =  InfineaEngineInterface()
    
    private var isStartTransaction =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //lib.addDelegate(self)
        self.Indicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        tableView.isScrollEnabled = false
        self.view.alpha = 0.85;
        NotificationCenter.default.addObserver(self, selector: #selector(BatteryCheck), name: NSNotification.Name(rawValue: "BatteryCheck"), object: nil)
        // Do any additional setup after loading the view.
        //
        self.paymentList.append(self.createPaymentItem(name: "Please present card.", color: UIColor.lightGray, status: NSNumber(value: false), inprog: NSNumber(value: false)))
        
        self.paymentList.append(self.createPaymentItem(name: "Card detected.", color: UIColor.lightGray, status: NSNumber(value: false), inprog: NSNumber(value: false)))
        
        self.paymentList.append(self.createPaymentItem(name: "Payment authorization.", color: UIColor.lightGray, status: NSNumber(value: false), inprog: NSNumber(value: false)))
        
        //self.paymentList.append(self.createPaymentItem(name: "Processing payment.", color: UIColor.lightGray, status: NSNumber(value: false), inprog: NSNumber(value: false)))
        self.paymentList.append(self.createPaymentItem(name: "Payment approving.", color: UIColor.lightGray, status: NSNumber(value: false), inprog: NSNumber(value: false)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let tot = dueAmount ?? 0
        //infinea.amount = tot * 100
        infinea.amount = 100
        infinea.startPaymentSetup(viewController: self, transaction: true)
    }
    
    @objc func startTransaction() {
        self.processColor(index: 1)
        infinea.startTransaction(transactionID: IETransactionTypeCreditSale)
    }
    
    @objc func BatteryCheck() {
        self.dismiss(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        infinea.removeObserver()
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let tableViewHeight = self.tableView.frame.height
        let contentHeight = self.tableView.contentSize.height
        
        let centeringInset = (tableViewHeight - contentHeight) / 2.0
        let topInset = max(centeringInset, 0.0)
        
        self.tableView.contentInset = UIEdgeInsets(top: topInset, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func createPaymentItem(name: String, color: UIColor, status: NSNumber, inprog: NSNumber) -> [String: Any] {
        var item = [String: Any]()
        item[kName] = name
        item[kPaymentColor] = color
        item[kStatus] = status
        item[kInprogress] = inprog
        return item
    }
    
    @IBAction func actionAbortProcess(_ sender: Any) {
        let item = self.paymentList.last
        if let status = item?[kStatus] as? NSNumber, !status.boolValue {
            self.dismiss(animated: true) {
            }
        }
    }
    
    @IBAction func actionDone(_ sender: Any) {
        if strProcessCompletion == "YES" {
            btn.isUserInteractionEnabled = true
            let process = self.paymentList[1]
            if let status = process[kStatus] as? NSNumber, status.boolValue {
            }
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Transcationupdate"), object: nil)
                self.infinea.removeObserver()
            }
        } else {
            btn.isUserInteractionEnabled = false
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func resetProcessTable(index: Int) {
        var item = self.paymentList[index]
        item[kPaymentColor] = UIColor( red: CGFloat(161/255.0), green: CGFloat(198/255.0), blue: CGFloat(61/255.0), alpha: CGFloat(1.0) )
        item[kStatus] = NSNumber(value: false)
        if index == 0 {
            item[kInprogress] = NSNumber(value: true)
        } else {
            item[kInprogress] = NSNumber(value: false)
        }
        self.paymentList[index] = item
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    //processColor
    func processColor(index: Int) {
        var item = self.paymentList[index]
        item[kPaymentColor] = UIColor( red: CGFloat(252.0/255.0), green: CGFloat(193.0/255.0), blue: CGFloat(60.0/255.0), alpha: CGFloat(1.0) )
        item[kInprogress] = NSNumber(value: true)
        self.paymentList[index] = item
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    func reloadProcessTable(index: Int) {
        var item = self.paymentList[index]
        item[kPaymentColor] = UIColor( red: CGFloat(161/255.0), green: CGFloat(198/255.0), blue: CGFloat(61/255.0), alpha: CGFloat(1.0) )
        item[kInprogress] = NSNumber(value: false)
        item[kStatus] = NSNumber(value: true)
        self.paymentList[index] = item
        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
    //extension CarPaymentViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var rect = tableView.frame
        rect.size.height = 55
        let view = UIView(frame: rect)
        view.backgroundColor = UIColor.clear
        
        let rect1 =  CGRect(x: 0, y: 0, width: rect.size.width, height: 50)
        let lbl = UILabel(frame: rect1)
        lbl.backgroundColor = UIColor.clear
        lbl.text = "CARD PAYMENT IN PROGRESS"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Montserrat-Regular", size: 20.0)
        lbl.textColor = UIColor.white
        view.addSubview(lbl)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var rect = tableView.frame
        rect.size.height = 100
        let view = UIView(frame: rect)
        view.backgroundColor = UIColor.black
        let rect1 =  CGRect(x: (tableView.bounds.width - 150) / 2, y: 20, width: 150, height: 60)
        btn.frame = rect1
        btn.setTitle("Done", for: .normal)
        btn.backgroundColor = UIColor.darkGray
        btn.titleLabel?.font  = UIFont(name: "Montserrat-Regular", size: 17.0)
        btn.layer.cornerRadius = 3.0
        btn.isOpaque = true
        btn.addTarget(self, action: #selector(self.actionDone(_:)), for: .touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardPaymentTableViewCell", for: indexPath) as! CardPaymentTableViewCell
        let item = self.paymentList[indexPath.row]
        
        if let name =  item[kName] as? String {
            cell.paymentlbl.text = name
        }
        
        if let status =  item[kInprogress] as? NSNumber,  status.boolValue, let color =  item[kPaymentColor] as? UIColor {
            cell.paymentBtn.setTitleColor(color, for: .normal)
            cell.paymentBtn.layer.borderColor = color.cgColor
            cell.paymentlbl.textColor = color
            cell.paymentBtn.setImage(UIImage(named: "In progress Icon"), for: .normal)
        }
        else if let status =  item[kStatus] as? NSNumber,  status.boolValue, let color =  item[kPaymentColor] as? UIColor {
            cell.paymentlbl.textColor = color
            cell.paymentBtn.setImage(UIImage(named: "Status check mark"), for: .normal)
            if indexPath.row == self.paymentList.count - 1 {
                strProcessCompletion = "YES"
                self.Indicator.stopAnimating()
                btn.backgroundColor = UIColor(red:12.0/255.0, green:88.0/255.0, blue:142.0/255.0, alpha:1.0)
            }
        } else {
            cell.paymentBtn.setImage(UIImage(named: "In Progress Deactivated"), for: .normal)
            cell.paymentlbl.textColor = UIColor.lightGray
        }
        
        return cell
    }
}

extension CarPaymentViewController: PaymentDeviceDelegation {
    func deviceTransactionCompleted(_ exportData: [AnyHashable : Any]?, transactionData: IPCTransactionData?) {
        
        if let amt = transactionData?.approvedAmt.getValueString(), amt.count > 0 {
            UserDefaults.standard.set(amt, forKey: "amountapproved")
            
        }
        if let last = transactionData?.last4Digits.getValueString(), last.count > 0 {
            DispatchQueue.main.async {
                self.Indicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
            UserDefaults.standard.set(last, forKey: "last4digits")
            
        }
        if let type = transactionData?.cardType.getValueString(), type.count > 0 {
            UserDefaults.standard.set(type, forKey: "cardtype")
            
        }
        if let dict = exportData as NSDictionary? as! [String:Any]? {
            UserDefaults.standard.set(dict["var.txn.issuerTxnId"] as Any, forKey: "Transcationid")
        }
        if let dict = exportData as NSDictionary? as! [String:Any]? {
            let status : String =  dict["var.txn.transactionResult"] as! String
            if status == "approval"{
                DispatchQueue.main.async {
                    self.reloadProcessTable(index: 0)
                    self.reloadProcessTable(index: 1)
                    self.reloadProcessTable(index: 2)
                    self.reloadProcessTable(index: 3)
                }
            }
        }
    }
    
    
    func deviceDidConnected() {
        
        if !isStartTransaction {
            isStartTransaction = true
            self.reloadProcessTable(index: 0)
            self.perform(#selector(self.startTransaction), with: nil, afterDelay: 1.5)
        }
    }
    
    func deviceDidDisConnected() {
        isStartTransaction = false
        self.resetProcessTable(index: 0)
        self.dismiss(animated: true)
    }
    
    func deviceTransactionProgress(_ action: IEAction?, with form: IEOperationForm?) {
        if let title = form?.title, title.hasPrefix("Insert/Tap/Swipe Card") {
            
            self.reloadProcessTable(index: 0)
            self.processColor(index: 1)
        }
        if let title = form?.title, title.hasPrefix("Please Wait...") {
            self.reloadProcessTable(index: 0)
            self.reloadProcessTable(index: 1)
            self.processColor(index: 2)
        }
        
        if let title = form?.title, title.hasSuffix("Insert Card") {
            self.reloadProcessTable(index: 0)
            self.processColor(index: 1)
            self.resetProcessTable(index: 2)
            ShowCustomAlert().showErrorAlrrt(messge: "Insert Card")
        }
        
        if let title = form?.title, title.hasPrefix("Declined") {
            errorcheck = "Declined"
            ShowCustomAlert().showErrorAlrrt(messge: form?.title ?? "")
        }
        
        if let title = form?.title, title.hasPrefix("Card Read Error") {
            self.reloadProcessTable(index: 0)
            self.processColor(index: 1)
            ShowCustomAlert().showErrorAlrrt(messge: form?.title ?? "")
        }
        
        if let title = form?.title, title.hasPrefix("TRY OTHER INTERFACE") {
            self.reloadProcessTable(index: 0)
            self.processColor(index: 1)
            ShowCustomAlert().showErrorAlrrt(messge: form?.title ?? "")
        }
        
        if let title = form?.title, title.hasPrefix("Transaction Success") {
            self.reloadProcessTable(index: 0)
            self.reloadProcessTable(index: 1)
            self.reloadProcessTable(index: 2)
            self.reloadProcessTable(index: 3)
        }
        else {
            
        }
    }
    
    func deviceTransactionCompleted(_ exportData: [AnyHashable : Any]?) {
        DispatchQueue.main.async {
            self.reloadProcessTable(index: 0)
            self.reloadProcessTable(index: 1)
            self.reloadProcessTable(index: 2)
            self.reloadProcessTable(index: 3)
        }
        if let dict = exportData as NSDictionary? as! [String:Any]? {
            print(dict["var.txn.amount"] as Any)
            print(dict["var.txn.authCode"] as Any)
            print(dict["var.txn.apprvAmt"] as Any)
            print(dict["var.txn.cardType"] as Any)
            print(dict["var.txn.txnID"] as Any)
            print(dict["var.txn.issuerTxnId"] as Any)
        }
    }
    
    func deviceTransactionFailure(_ error: Error?, _ transactionData: IPCTransactionData?) {
        DispatchQueue.main.async {
            self.Indicator.stopAnimating()
            if self.errorcheck == "Declined"{
                self.errorcheck = ""
            }else{
ShowCustomAlert().showErrorAlrrt(messge: "Transaction Timeout or Transaction Cancelled.")
            }
        }
        self.dismiss(animated: true)
    }
    
}

